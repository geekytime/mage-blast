using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharM3_Knight : CharM3
{
    private string[] attack_animations;
    private string[] defense_animations;

    // Use this for initialization
    void Start ()
    {
        Inits();
        animations = new string[] { "attack", "attack3", "attack4", "battlecry",
            "dying", "idle", "jump","run","shield","walk"};
        attack_animations = new string[] { "attack", "attack3", "attack4" };
        defense_animations = new string[] { "battlecry", "shield" };

        hp = 20;
        mana = 10;
        ac = 50;
    }

    protected override void TakeClassAction(EAction action, Vector2 details)
    {
        switch (action)
        {
            /*
            case "attack":
                animator.Play(attack_animations[Random.Range(0, attack_animations.Length)]);
                break;
            case "defense":
                animator.Play(defense_animations[Random.Range(0, defense_animations.Length)]);
                break;
            */        
            default:
                animator.Play("idle");
                rB2D.drag = startingDrag * 100;
                break;
        }

    }
}
