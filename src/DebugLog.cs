﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class DebugLog : MonoBehaviour
{
    public InputField logText;
    private string debugLog;

    private string path;

    // Start is called before the first frame update
    void Start()
    {
        debugLog = "\nDebug Log created at " + System.DateTime.Now.ToString() + "\n";

        path = Application.persistentDataPath + "/DebugLog.txt";
        logText.text = path;
    }

    public void LogMessage(string message)
    {
        if (debugLog.Length > 1000000000) // 1,000,000,000
        {
            debugLog = "\nDebug Log created at " + System.DateTime.Now.ToString() + "\n";
            debugLog += " LOG LENGTH EXCEEDED 1GB.  LOG PURGED TO PRESERVE SPACE.  SORRY\n";
        }

        debugLog += System.DateTime.Now.ToString() + ":  " + message + "\n";
    }

    public string GetLog()
    {
        return debugLog;
    }

    public void PrintLog()
    {
        try
        {
            Debug.Log("DebugLog printed to: " + path);
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Create(path);
            bf.Serialize(file, debugLog);
            file.Close();
        }
        catch (Exception e)
        {
            Debug.Log("DebugLog FAILED TO PRINT: " + e);
            logText.text = "error: " + e;
        }
    }
}
