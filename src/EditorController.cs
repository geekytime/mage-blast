using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Jurassic;
using Jurassic.Library;
using System.Linq;

public class EditorController : MonoBehaviour
{
    public AccessSaveData accessSaveData;
    public MageEntrySetup mageEntrySetup;
    public CanvasSlider saveWarningCanvas;
    public CanvasSlider jsCanvas;

    public InputField inputField_JS;
    public InputField inputField_mageName;

    public Text errorText;

    public Button saveButton;
    public Button menuButton;

    private bool unsavedEdits;
    private int currentBucket;

    private ScriptEngine jurassicEngine;

    public MageData myMage;

    // Start is called before the first frame update
    void Start()
    {
        // Create an instance of the Jurassic engine then expose some stuff to it.
        jurassicEngine = new Jurassic.ScriptEngine();

        myMage = new MageData();

        // Expose C# custom type to Jurassic
        jurassicEngine.EnableExposedClrTypes = true;
        jurassicEngine.SetGlobalValue("MageData", typeof(MageData));

        // Arguments and returns of functions exposed to JavaScript must be of supported types.
        // Supported types are bool, int, double, string, Jurassic.Null, Jurassic.Undefined
        // Declare INPUT functions exposed to Jurassic
        jurassicEngine.SetGlobalFunction("SetAbilityAction", new System.Action<string, int>(SetAbilityAction));
        jurassicEngine.SetGlobalFunction("SetMoveAction", new System.Action<bool, int>(SetMoveAction));

        jurassicEngine.SetGlobalFunction("SetRandomAttack", new System.Action<bool>(SetRandomAttack));
        jurassicEngine.SetGlobalFunction("SetRandomMove", new System.Action<bool>(SetRandomMove));

        // TESTING FUNCTION!
        jurassicEngine.SetGlobalFunction("DebugLog", new System.Action<string>(DebugLog));

        // Declare RETURN functions exposed to Jurassic
        jurassicEngine.SetGlobalFunction("GetCurrentOpponentCount", new System.Func<int>(GetCurrentOpponentCount));
        jurassicEngine.SetGlobalFunction("GetClosestOpponent_ID", new System.Func<int>(GetClosestOpponent_ID));
        jurassicEngine.SetGlobalFunction("GetLowestHealthOpponent_ID", new System.Func<int>(GetLowestHealthOpponent_ID));

        Reload();
    }

    public void DebugLog(string printThis){}
    public void SetAbilityAction(string actionIn, int dataIn){}
    public void SetAbilityAction(string actionIn){}
    public void SetMoveAction(bool move, int angle){}
    public void SetMoveAction(bool move){}

    public int GetCurrentOpponentCount(){return 9;}
    public int GetClosestOpponent_ID(){return 10001; }
    public int GetLowestHealthOpponent_ID(){return 10001; }
    public void SetRandomAttack(bool whatever) {}
    public void SetRandomMove(bool whatever) { }

    private void SetGlobalVariablesInJS()
    {
        jurassicEngine.SetGlobalValue("myMage", myMage);

        // each element within MageData is loaded as an individual array, as opposed to an array of mageData
        // This is due to Jurassic limitations, which does not allow an array of non-primitives

        jurassicEngine.SetGlobalValue("enemy_id", NewArray(new int[]{
            10001,
            20002,
            30003,
            40004,
            50005,
            60006,
            70007,
            80008,
            90009}));

        jurassicEngine.SetGlobalValue("enemy_x", NewArray(new float[]{
            UnityEngine.Random.Range(1, 10.1f),
            UnityEngine.Random.Range(1, 10.1f),
            UnityEngine.Random.Range(1, 10.1f),
            UnityEngine.Random.Range(1, 10.1f),
            UnityEngine.Random.Range(1, 10.1f),
            UnityEngine.Random.Range(1, 10.1f),
            UnityEngine.Random.Range(1, 10.1f),
            UnityEngine.Random.Range(1, 10.1f),
            UnityEngine.Random.Range(1, 10.1f)}));

        jurassicEngine.SetGlobalValue("enemy_y", NewArray(new float[]{
            UnityEngine.Random.Range(1, 7.1f),
            UnityEngine.Random.Range(1, 7.1f),
            UnityEngine.Random.Range(1, 7.1f),
            UnityEngine.Random.Range(1, 7.1f),
            UnityEngine.Random.Range(1, 7.1f),
            UnityEngine.Random.Range(1, 7.1f),
            UnityEngine.Random.Range(1, 7.1f),
            UnityEngine.Random.Range(1, 7.1f),
            UnityEngine.Random.Range(1, 7.1f)}));

        jurassicEngine.SetGlobalValue("enemy_hp", NewArray(new int[]{
            UnityEngine.Random.Range(0, 11),
            UnityEngine.Random.Range(0, 11),
            UnityEngine.Random.Range(0, 11),
            UnityEngine.Random.Range(0, 10),
            UnityEngine.Random.Range(0, 10),
            UnityEngine.Random.Range(0, 10),
            UnityEngine.Random.Range(0, 10),
            UnityEngine.Random.Range(0, 10),
            UnityEngine.Random.Range(0, 10)}));

        jurassicEngine.SetGlobalValue("enemy_mana", NewArray(new int[]{
            UnityEngine.Random.Range(0, 11),
            UnityEngine.Random.Range(0, 11),
            UnityEngine.Random.Range(0, 11),
            UnityEngine.Random.Range(0, 10),
            UnityEngine.Random.Range(0, 10),
            UnityEngine.Random.Range(0, 10),
            UnityEngine.Random.Range(0, 10),
            UnityEngine.Random.Range(0, 10),
            UnityEngine.Random.Range(0, 10)}));

        jurassicEngine.SetGlobalValue("enemy_isShieldOn", NewArray(new bool[]{
            RandomBool(),
            RandomBool(),
            RandomBool(),
            RandomBool(),
            RandomBool(),
            RandomBool(),
            RandomBool(),
            RandomBool(),
            RandomBool()}));
    }

    private bool RandomBool()
    {
        if (UnityEngine.Random.Range(0, 2) == 0)
            return true;
        else
            return false;
    }

    // Helper function to convert a .NET collection into an array
    // in the script engine
    private ArrayInstance NewArray<T>(IEnumerable<T> data)
    {
        return jurassicEngine.Array.New(data.Cast<object>().ToArray());
    }

    public bool ValidateScripts()
    {
        bool valid = true;
        errorText.text = "";

                try
                {
                    SetGlobalVariablesInJS();
                    jurassicEngine.Execute(inputField_JS.text);
                }

                catch (JavaScriptException e)
                {
                    valid = false;
                    errorText.text = e.ToString();
                    Debug.Log("VALIDATION FAILED, JavaScriptException was caught: " + e.ToString());
                }

                catch (Exception e)
                {
                    valid = false;
                    errorText.text = e.ToString();
                    Debug.Log("VALIDATION FAILED, Generic Exception was caught: " + e.ToString());
                }
        
        return valid;
    }

    public void Reload()
    {
        errorText.text = "";

        currentBucket = mageEntrySetup.CurrentBucket();

        inputField_JS.text = accessSaveData.GetJS(currentBucket);
        inputField_mageName.text = accessSaveData.GetMageName(currentBucket);

        unsavedEdits = false;
    }

    public void SaveEdits()
    {
        bool scriptValidated = true;
        int loopcounter = 0;

        while(scriptValidated && loopcounter < 100)
        {
            scriptValidated = ValidateScripts();
            loopcounter++;
        }

        if (scriptValidated)
        {
            accessSaveData.SetJS(currentBucket, inputField_JS.text);
            accessSaveData.SetMageName(currentBucket, inputField_mageName.text);

            SetUnsavedEdits(false);
        }
    }

    public void SetUnsavedEdits(bool setTo)
    {
        unsavedEdits = setTo;
    }

    public bool GetUnsavedEdits()
    {
        return unsavedEdits;
    }

    public void OnBackButton()
    {
        if (unsavedEdits)
        {
            saveWarningCanvas.ActivateCanvas();
        }
        else
            jsCanvas.ExitCanvas();
    }

    public void InitializeJS()
    {
        accessSaveData.ResetMageBucket(currentBucket);
        Reload();
    }
}
