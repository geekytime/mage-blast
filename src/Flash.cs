using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flash : MonoBehaviour
{

    public SpriteRenderer flashImage;
    public float flashDuration; // in seconds
    public float intensity; // 0.0 - 1.0

    // Start is called before the first frame update
    void Start()
    {
        flashImage.color = new Color(1, 1, 1, 0);

        if (intensity > 1)
            intensity = 1;
        if (intensity < 0)
            intensity = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (flashImage.color.a > 0)
            flashImage.color = new Color(1,1,1,flashImage.color.a - (Time.deltaTime / flashDuration));
    }

    public void ActivateFlash()
    {
        flashImage.color = new Color(1, 1, 1, intensity);
    }
}
