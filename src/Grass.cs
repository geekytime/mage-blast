using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grass : MonoBehaviour
{
    public Sprite[] grassSprite;

    private int spriteCounter;
    private float imageTimer;

    public bool isMoving;

    // Start is called before the first frame update
    void Start()
    {
        spriteCounter = 0;
        imageTimer = 0.025f;
        isMoving = false;
    }

    void OnTriggerEnter2D(Collider2D trigger)
    {
        MoveGrass(true);
    }

    public void MoveGrass(bool movingRight)
    {
        if (!isMoving)
        {
            StartCoroutine(GrassAnimation(movingRight));
        }
    }

    public void FlipGrass()
    {
        this.GetComponent<SpriteRenderer>().flipX = !this.GetComponent<SpriteRenderer>().flipX;
    }

    public IEnumerator GrassAnimation(bool movingRight)
    {

        spriteCounter = 0;
        isMoving = true;

        while (spriteCounter < grassSprite.Length)
        {
            this.GetComponent<SpriteRenderer>().sprite = grassSprite[spriteCounter];
            spriteCounter++;

            yield return new WaitForSeconds(imageTimer);
        }
       
        // and rebound grass back to normal
        while (spriteCounter > 0)
        {
            spriteCounter--;
            this.GetComponent<SpriteRenderer>().sprite = grassSprite[spriteCounter];

            yield return new WaitForSeconds(imageTimer);
        }

        FlipGrass();

        if (grassSprite.Length > 1)
        {
            this.GetComponent<SpriteRenderer>().sprite = grassSprite[1];
            yield return new WaitForSeconds(imageTimer);
            this.GetComponent<SpriteRenderer>().sprite = grassSprite[0];
            yield return new WaitForSeconds(imageTimer);
        }

        if (movingRight)
            FlipGrass();

        isMoving = false;
    }
}
