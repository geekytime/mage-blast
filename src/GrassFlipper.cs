using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrassFlipper : MonoBehaviour
{
    private Grass grass;
    // Start is called before the first frame update
    void Start()
    {
        grass = GetComponentInParent<Grass>();
    }

    void OnTriggerEnter2D(Collider2D trigger)
    {
        if (!grass.isMoving)
        {
            grass.FlipGrass(); // invert image so it moves left, instead of right
            grass.MoveGrass(false); // go through teh normal animation, but pass 'false' undering teh moving right
            // variable to NOT flip the grass when animation is complete
        }
    }
}