using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Jurassic;
using Jurassic.Library;
using System.Linq;

//using ServiceStack.Script;

public class MageControllerJS : MonoBehaviour
{
    public DebugLog debugLog;
    private ScriptEngine jurassicEngine;

    private EAction action;
    private int movementAngle;
    private int attackID;

    private List<MageData> currentMageData = new List<MageData>();
    public MageData myMage;
    private System.Random randomNum;
    private bool takeMoveAction;

    private string customJS;

    // Start is invoked once during program load
    public void Start()
    {
        debugLog = FindObjectOfType<DebugLog>();

        // Create an instance of the Jurassic engine then expose some stuff to it.
        jurassicEngine = new Jurassic.ScriptEngine();

        myMage = new MageData();

        // Expose C# custom type to Jurassic
        jurassicEngine.EnableExposedClrTypes = true;
        jurassicEngine.SetGlobalValue("MageData", typeof(MageData));

        // Arguments and returns of functions exposed to JavaScript must be of supported types.
        // Supported types are bool, int, double, string, Jurassic.Null, Jurassic.Undefined
        // Declare INPUT functions exposed to Jurassic
        jurassicEngine.SetGlobalFunction("SetAbilityAction", new System.Action<string, int>(SetAbilityAction));
        jurassicEngine.SetGlobalFunction("SetMoveAction", new System.Action<bool, int>(SetMoveAction));

        jurassicEngine.SetGlobalFunction("SetRandomAttack", new System.Action<bool>(SetRandomAttack));
        jurassicEngine.SetGlobalFunction("SetRandomMove", new System.Action<bool>(SetRandomMove));

        // TESTING FUNCTION!
        jurassicEngine.SetGlobalFunction("DebugLog", new System.Action<string>(DebugLog));

        // Declare RETURN functions exposed to Jurassic
        jurassicEngine.SetGlobalFunction("GetCurrentOpponentCount", new System.Func<int>(GetCurrentOpponentCount));
        jurassicEngine.SetGlobalFunction("GetClosestOpponent_ID", new System.Func<int>(GetClosestOpponent_ID));
        jurassicEngine.SetGlobalFunction("GetLowestHealthOpponent_ID", new System.Func<int>(GetLowestHealthOpponent_ID));

        NewTurn();
        randomNum = new System.Random(DateTime.Now.Millisecond + 29);
    }

    public void DebugLog(string printThis)
    {
        debugLog.LogMessage(printThis);
        Debug.Log("JS Debugger: " + printThis);
    }

    public void SetAbilityAction(string actionIn, int dataIn)
    {
        switch (actionIn.ToLower())
        {
            case "blink":
                action = EAction.blink;
                movementAngle = dataIn;
                break;
            case "drainmana":
                action = EAction.drainmana;
                attackID = dataIn;
                break;
            case "frostbolt":
                action = EAction.frostbolt;
                attackID = dataIn;
                break;
            case "iceshield":
                action = EAction.iceshield;
                break;
            case "snowball":
                action = EAction.snowball;
                attackID = dataIn;
                break;
            default:
                action = EAction.nothing;
                break;
        }
    }

    public void SetMoveAction(bool move, int angle)
    {
        takeMoveAction = move;
        movementAngle = angle;
    }

    public int GetCurrentOpponentCount()
    {
        int aliveCount = 0;

        for (int i = 0; i < currentMageData.Count; i++)
        {
            if (currentMageData[i].hp > 0)
                aliveCount++;
        }
        return aliveCount;
    }

    public int GetClosestOpponent_ID()
    {
        MageData closestOpponent = currentMageData[0];

        float closestDistance = 1000;
        Vector2 opponentPosition;
        Vector2 myPosition = new Vector2(myMage.x, myMage.y);

        for (int i = 0; i < currentMageData.Count; i++)
        {
            opponentPosition = new Vector2(currentMageData[i].x, currentMageData[i].y);

            if (Vector2.Distance(myPosition, opponentPosition) < closestDistance)
            {
                closestOpponent = currentMageData[i];
                closestDistance = Vector2.Distance(myPosition, opponentPosition);
            }
        }

        return closestOpponent.id;
    }

    public int GetLowestHealthOpponent_ID()
    {
        MageData lowestOpponent = currentMageData[0];

        float lowestHealth = 100;

        for (int i = 0; i < currentMageData.Count; i++)
        {
            if (currentMageData[i].hp < lowestHealth)
            {
                lowestOpponent = currentMageData[i];
                lowestHealth = currentMageData[i].hp;
            }
        }

        return lowestOpponent.id;
    }

    public void SetRandomAttack(bool whatever)
    {
        switch (randomNum.Next(1, 7))
        {
            case 1:
                action = EAction.blink;
                SelectRandomAngle();
                break;
            case 2:
                action = EAction.drainmana;
                SelectRandomTarget();
                break;
            case 3:
                action = EAction.frostbolt;
                SelectRandomTarget();
                break;
            case 4:
                action = EAction.iceshield;
                break;
            case 5:
                action = EAction.snowball;
                SelectRandomTarget();
                break;
            default:
                action = EAction.nothing;
                SelectRandomTarget();
                break;
        }
    }

    public void SetRandomMove(bool whatever)
    {
        takeMoveAction = true;
        SelectRandomAngle();
    }

    /********************************************************************/
    /********************************************************************/
    /********************************************************************/

    #region INTERNAL SCRIPTS USED WITHIN C#

    /********************************************************************/
    /********************************************************************/
    /********************************************************************/

    public void SetScripts(string customJSIn)
    {
        customJS = customJSIn;
    }

    // LoadListData will be invoked multiple times per turn, once for each mage left alive (NOT including yourself)
    public void LoadListData(MageData inData)
    {
        //Debug.Log("LoadListData:" + myMage.id + " " + inData.id);
        currentMageData.Add(inData);
    }

    public void LoadMyMageData(MageData inData)
    {
        //Debug.Log("LoadMyMageData " + inData.id);
        myMage = inData;
    }

    // NewTurn is invoked at the start of each turn.  Reset anything you'd like, or maybe make a copy of the prior
    // list to use for trending purposes?
    public void NewTurn()
    {
        currentMageData = new List<MageData>();

        action = EAction.nothing;
        movementAngle = 0;
        attackID = 0;
    }

    public void ExecuteScripts()
    {
       try
        {
            SetGlobalVariablesInJS();
            jurassicEngine.Execute(customJS);
        }

        catch (JavaScriptException e)
        {
            debugLog.LogMessage("JavaScriptException was caught: " + e);
            Debug.Log("JavaScriptException was caught: " + e);
        }

        catch (Exception e)
        {
            debugLog.LogMessage("Generic Exception caught: " + e);
            Debug.Log("Generic Exception was caught: " + e);
        }
    }

    private void SetGlobalVariablesInJS()
    {
        jurassicEngine.SetGlobalValue("myMage", myMage);
        //Debug.Log("currentMageData.Count(): " + currentMageData.Count());

        // each element within MageData is loaded as an individual array, as opposed to an array of mageData
        // This is due to Jurassic limitations, which does not allow an array of non-primitives

        List<int> id_list = new List<int>();
        List<float> x_list = new List<float>();
        List<float> y_list = new List<float>();
        List<int> hp_list = new List<int>();
        List<int> mana_list = new List<int>();
        List<bool> isShieldOn_list = new List<bool>();

        for (int i = 0; i< currentMageData.Count(); i++)
        {
            id_list.Add(currentMageData[i].id);
            x_list.Add(currentMageData[i].x);
            y_list.Add(currentMageData[i].y);
            hp_list.Add(currentMageData[i].hp);
            mana_list.Add(currentMageData[i].mana);
            isShieldOn_list.Add(currentMageData[i].isShieldOn);
        }

        int[] id_array = id_list.ToArray();
        float[] x_array = x_list.ToArray();
        float[] y_array = y_list.ToArray();
        int[] hp_array = hp_list.ToArray();
        int[] mana_array = mana_list.ToArray();
        bool[] isShieldOn_array = isShieldOn_list.ToArray();

        jurassicEngine.SetGlobalValue("enemy_id", NewArray(id_array));
        jurassicEngine.SetGlobalValue("enemy_x", NewArray(x_array));
        jurassicEngine.SetGlobalValue("enemy_y", NewArray(y_array));
        jurassicEngine.SetGlobalValue("enemy_hp", NewArray(hp_array));
        jurassicEngine.SetGlobalValue("enemy_mana", NewArray(mana_array));
        jurassicEngine.SetGlobalValue("enemy_isShieldOn", NewArray(isShieldOn_array));

    }

// Helper function to convert a .NET collection into an array
    // in the script engine
    private ArrayInstance NewArray<T>(IEnumerable<T> data)
    {
        return jurassicEngine.Array.New(data.Cast<object>().ToArray());
    }

    public EAction GetTurnAction()
    {
        return action;
    }

    public int GetAttackID()
    {
        return attackID;
    }

    public bool GetMoveThisTurn()
    {
        return takeMoveAction;
    }

    public int GetMovementAngle()
    {
        return movementAngle;
    }

    private void SelectRandomAngle()
    {
        movementAngle = UnityEngine.Random.Range(1, 361);
    }

    private void SelectRandomTarget()
    {
//        Debug.Log("SelectRandomTarget currentMageData.Count: " + currentMageData.Count);
        MageData tempMage = new MageData();
        int rando = UnityEngine.Random.Range(0, currentMageData.Count);
        tempMage = currentMageData[rando];
        attackID = tempMage.id;
    }

    public int GetMageID()
    {
        return myMage.id;
    }
    #endregion
}

