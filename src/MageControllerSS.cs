using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using ServiceStack.Script;

public class MageControllerSS : MonoBehaviour
{
    public DebugLog debugLog;

    private EAction action;
    private int movementAngle;
    private int attackID;

    private List<MageData> currentMageData = new List<MageData>();
    public MageData myMage;
    private bool takeMoveAction;
    private System.Random randomNum;

    private string attackScript;
    private string moveScript;

    private ScriptContext script;
    private Lisp.Interpreter lisp;

    public class UnityScripts : ScriptMethods
    {
        public string TestPrint(string inString)
        {
            Debug.Log("TestPrint test success + " + inString);
            return "yup";
        }
    }

    // Start is invoked once during program load
    public void Start()
    {
        debugLog = FindObjectOfType<DebugLog>();

        Lisp.Interpreter lisp;
        string txtReplIn;
        string textReplOut;

        script = new ScriptContext
        {
            ScriptLanguages = {
                ScriptLisp.Language
                //SharpScript.Language
            },
            ScriptMethods = {
                new ProtectedScripts(),
                new UnityScripts(),
            },
            AllowScriptingOfAllTypes = true,
            ScriptNamespaces = {
                nameof(UnityEngine)
            },
            Args = {
                ["arg1"] = 1,
                ["arg2"] = 2,
            }
        }.Init();

        lisp = Lisp.CreateInterpreter();


    }

    public void DebugLog(string printThis)
    {
        debugLog.LogMessage(printThis);
        Debug.Log("Script Debugger: " + printThis);
    }

    public void SetAbilityAction(string actionIn, int dataIn)
    {
        switch (actionIn.ToLower())
        {
            case "blink":
                action = EAction.blink;
                movementAngle = dataIn;
                break;
            case "drainmana":
                action = EAction.drainmana;
                attackID = dataIn;
                break;
            case "frostbolt":
                action = EAction.frostbolt;
                attackID = dataIn;
                break;
            case "iceshield":
                action = EAction.iceshield;
                break;
            case "snowball":
                action = EAction.snowball;
                attackID = dataIn;
                break;
            default:
                action = EAction.nothing;
                break;
        }
    }

    public void SetMoveAction(bool move, int angle)
    {
        takeMoveAction = move;
        movementAngle = angle;
    }

    public int GetCurrentOpponentCount()
    {
        return currentMageData.Count;
    }

    public int GetClosestOpponent_ID()
    {
        MageData closestOpponent = currentMageData[0];

        float closestDistance = 1000;
        Vector2 opponentPosition;
        Vector2 myPosition = new Vector2(myMage.x, myMage.y);

        for (int i = 0; i < currentMageData.Count; i++)
        {
            opponentPosition = new Vector2(currentMageData[i].x, currentMageData[i].y);

            if (Vector2.Distance(myPosition, opponentPosition) < closestDistance)
            {
                closestOpponent = currentMageData[i];
                closestDistance = Vector2.Distance(myPosition, opponentPosition);
            }
        }

        return closestOpponent.id;
    }

    public int GetLowestHealthOpponent_ID()
    {
        MageData lowestOpponent = currentMageData[0];

        float lowestHealth = 100;

        for (int i = 0; i < currentMageData.Count; i++)
        {
            if (currentMageData[i].hp < lowestHealth)
            {
                lowestOpponent = currentMageData[i];
                lowestHealth = currentMageData[i].hp;
            }
        }

        return lowestOpponent.id;
    }

    public void SetRandomAttack(bool whatever)
    {
        switch (randomNum.Next(1, 7))
        {
            case 1:
                action = EAction.blink;
                SelectRandomAngle();
                break;
            case 2:
                action = EAction.drainmana;
                SelectRandomTarget();
                SelectRandomAngle();
                break;
            case 3:
                action = EAction.frostbolt;
                SelectRandomTarget();
                SelectRandomAngle();
                break;
            case 4:
                action = EAction.iceshield;
                SelectRandomAngle();
                break;
            case 5:
                action = EAction.snowball;
                SelectRandomTarget();
                SelectRandomAngle();
                break;
            default:
                action = EAction.nothing;
                SelectRandomTarget();
                SelectRandomAngle();
                break;
        }
    }

    public void SetRandomMove(bool whatever)
    {
        takeMoveAction = true;
        SelectRandomAngle();
    }

    /*
    private void SetMageData(MageData mageIn)
    {
        Debug.Log("SetMageData");
        Debug.Log("mageIn.hp:   " + mageIn.hp);
        Debug.Log("mageIn.mana: " + mageIn.mana);
        Debug.Log("mageIn.id:   " + mageIn.id);
        Debug.Log("mageIn.x:    " + mageIn.x);
        Debug.Log("mageIn.y:    " + mageIn.y);
    }
    */
    /********************************************************************/
    /********************************************************************/
    /********************************************************************/

    #region INTERNAL SCRIPTS USED WITHIN C#

    /********************************************************************/
    /********************************************************************/
    /********************************************************************/

    public void SetScripts(string attackIn, string moveIn)
    {
        attackScript = attackIn;
        moveScript = moveIn;
    }

    // LoadListData will be invoked multiple times per turn, once for each mage left alive (NOT including yourself)
    // This is your feed of data.  While you are free to add whatever functionality you would like, be sure to 
    // maintain the ability to receive inData from multiple iterations before data is reset next turn
    public void LoadListData(MageData inData)
    {
        //Debug.Log("LoadListData:" + myMage.id + " " + inData.id);
        currentMageData.Add(inData);
    }

    public void LoadMyMageData(MageData inData)
    {
        //Debug.Log("LoadMyMageData " + inData.id);
        myMage = inData;
    }

    // NewTurn is invoked at the start of each turn.  Reset anything you'd like, or maybe make a copy of the prior
    // list to use for trending purposes?
    public void NewTurn()
    {
        currentMageData = new List<MageData>();

        action = EAction.nothing;
        movementAngle = 0;
        attackID = 0;
    }

    public void ExecuteScripts()
    {

        Debug.Log("Executing Scripts");

        try
        {
            //Debug.Log("lisp.ReplEval: " + lisp.ReplEval(script, Stream.Null, attackScript));
            //lisp.Eval(txtReplIn);
            //textReplOut = lisp.ReplEval(script, Stream.Null, txtReplIn);
            //Debug.Log("Try attempt success: " + textReplOut);
        }
        catch (Exception e)
        {
            Debug.Log("Error Caught: " + e);
        }

        try
        {
            Debug.Log("script.RenderCode: " + script.RenderCode(attackScript));
            //lisp.Eval(txtReplIn);
            //textReplOut = lisp.ReplEval(script, Stream.Null, txtReplIn);
            //Debug.Log("Try attempt success: " + textReplOut);
        }
        catch (Exception e)
        {
            Debug.Log("Error Caught: " + e);
        }

        /*       try
                {
                    SetGlobalVariablesInJS();
                    jurassicEngine.Execute(attackScript);

                    SetGlobalVariablesInJS();
                    jurassicEngine.Execute(moveScript);
                }

                catch (JavaScriptException e)
                {
                    debugLog.LogMessage("JavaScriptException was caught: " + e);
                    Debug.Log("JavaScriptException was caught: " + e);
                }

                catch (Exception e)
                {
                    debugLog.LogMessage("Generic Exception caught: " + e);
                    Debug.Log("Generic Exception was caught: " + e);
                }
        */
    }

    private void SetGlobalVariablesInJS()
    {
/*        string jurassicVariableName;
        MageData lowValues = new MageData();

        jurassicEngine.SetGlobalValue("myMage", myMage);

        for (int i = 0; i < 10; i++)
        {
            jurassicVariableName = "mage" + i.ToString();
            jurassicEngine.SetGlobalValue(jurassicVariableName, lowValues);
        }

        for (int i = 0; i < currentMageData.Count; i++)
        {
            jurassicVariableName = "mage" + i.ToString();
            jurassicEngine.SetGlobalValue(jurassicVariableName, currentMageData[i]);
        }
*/    }

    public EAction GetTurnAction()
    {
        return action;
    }

    public int GetAttackID()
    {
        return attackID;
    }

    public bool GetMoveThisTurn()
    {
        return takeMoveAction;
    }

    public int GetMovementAngle()
    {
        return movementAngle;
    }

    private void SelectRandomAngle()
    {
        movementAngle = UnityEngine.Random.Range(1, 361);
    }

    private void SelectRandomTarget()
    {
        Debug.Log("SelectRandomTarget currentMageData.Count: " + currentMageData.Count);
        MageData tempMage = new MageData();
        int rando = UnityEngine.Random.Range(0, currentMageData.Count);
        tempMage = currentMageData[rando];
        attackID = tempMage.id;
    }

    public int GetMageID()
    {
        return myMage.id;
    }
    #endregion
}

