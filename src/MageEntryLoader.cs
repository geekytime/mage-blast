using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MageEntryLoader : MonoBehaviour
{
    public AccessSaveData accessSaveData;
    public MageEntrySetup mageEntrySetup;

    public int thisMageNum;
    public Text nameText;
    public Button addCustomMageButton;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(LoadName());

        if (mageEntrySetup.BucketActive(thisMageNum))
            addCustomMageButton.onClick.Invoke();

    }

    IEnumerator LoadName()
    {
        while (true)
        {
            nameText.text = accessSaveData.GetMageName(thisMageNum);
            yield return new WaitForSeconds(1f);
        }
    }
}
