using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SaveButton : MonoBehaviour
{
    public EditorController editorController;

    private Button thisButton;

    // Start is called before the first frame update
    void Start()
    {
        thisButton = GetComponent<Button>();

        StartCoroutine(CheckButtonStatus());
    }

    IEnumerator CheckButtonStatus()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.25f);

            if (editorController.GetUnsavedEdits())
                thisButton.interactable = true;
            else
                thisButton.interactable = false;
        }
    }
}
