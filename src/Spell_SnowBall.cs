using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spell_SnowBall : Spell
{
    private Transform snowballImage;

    // Start is called before the first frame update
    void Start()
    {
        snowballImage = GetComponent<Transform>();
        circleCollider = GetComponent<CircleCollider2D>();

        circleCollider.isTrigger = false;
        show = false;

        damage = 3;
    }

    // Update is called once per frame
    void Update()
    {
        if (show)
            MoveBolt();
    }

    public int ManaCost()
    {
        return 3;
    }
}
